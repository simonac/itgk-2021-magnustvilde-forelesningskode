'''Pseudokode'''

# Eksempel med pseudokode. Skriv pseudokode - gjør oppgaven etterpå.

# Oppgave:
'''
Fem eldre brødre og en lillebror selger juletrær. De har bestemt seg for at betalingen skal
fordeles på følgende måte: lillebror får restverdien etter at hver hele 100-lapp har blitt
fordelt mellom de eldre brødrene.
Brødrene selger 4 trær der bruker skal skrive inn hva det første treet ble solgt for. For
hvert juletre som blir solgt ser de at forespørselen går opp, og setter derfor opp prisen
med 10%. Skriv til skjermen hvor mye brødrene får i lønn.
'''

# hent inn pris det første juletreet ble solgt for
pris = input('Skriv inn hvor mye det første treet ble solgt for: ')
# gjør om til float
pris = float(pris)

# lag en variabel som holder styr på hvor mye penger brødrene har tjent
total = 0

# legg til prisen til total mengde penger tjent
total = total + pris

#2: gang prisen med 1.1
pris = pris * 1.1
# legg til prisen i variabelen
total += pris

#3: gang prisen med 1.1
pris *= 1.1
# legg til prisen i variabelen
total += pris

#4: gang prisen med 1.1
pris = pris * 1.1
# legg til prisen i variabelen
total += pris

# finn ut av hvor mye lønn lillebror får, regn ut modulo
lillebrorLonn = total%100

# trekk fra lillebror lønn fra total
total = total - lillebrorLonn

# fordel total mellom storebrødre (del på 5)
lonnStorebror = total/5

# print ut resultatet
print(f'Lillebror sin lønn: {lillebrorLonn}')
print(f'Storebrødre sin lønn: {lonnStorebror}')
